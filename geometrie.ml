type point = { x : int; y : int} ;;

let new_point x y =
  { x = x; y = y}
;;

type quadrilatere = {
  p1 : point;
  p2 : point;
  p3 : point;
  p4 : point
} ;;

let new_quadrilatere p1 p2 p3 p4 =
{
  p1 = p1;
  p2 = p2;
  p3 = p3;
  p4 = p4
} ;;

let vect p1 p2 =
  {x = p2.x - p1.x; y = p2.y - p1.y}
;;

let vects quadrilatere =
  vect quadrilatere.p1 quadrilatere.p2,
  vect quadrilatere.p2 quadrilatere.p3,
  vect quadrilatere.p3 quadrilatere.p4,
  vect quadrilatere.p4 quadrilatere.p1
;;

let prod_scal v1 v2 =
  v1.x * v2.x + v1.y * v2.y
;;

let long_vec v =
  v.x * v.x + v.y * v.y
;;

let is_rectangle quadrilatere = 
  let v1, v2, v3, v4 = vects quadrilatere in
  (prod_scal v1 v2) = 0 &&
  (prod_scal v2 v3) = 0 &&
  (prod_scal v3 v4) = 0
;;

let is_losange quadrilatere =
  let v1, v2, v3, v4 = vects quadrilatere in
  long_vec v1 = long_vec v2 &&
  long_vec v2 = long_vec v3 &&
  long_vec v3 = long_vec v4 &&
  long_vec v4 = long_vec v1
;;

let is_carre quadrilatere =
  is_rectangle quadrilatere && is_losange quadrilatere
;;

type figure = 
  | Rectangle of quadrilatere
  | Losange of quadrilatere
  | Carre of quadrilatere
  | Autre of quadrilatere
;;

let to_figure quadrilatere =
  if is_carre quadrilatere then
    Carre quadrilatere
  else
    if is_rectangle quadrilatere then
      Rectangle quadrilatere
    else
      if is_losange quadrilatere then
        Losange quadrilatere
      else
        Autre quadrilatere
;;

let compare f1 f2 =
   match f1, f2 with
    | Carre(f1), Carre(f2) -> 0
    | Rectangle(f1), Rectangle(f2) -> 0
    | Losange(f1), Losange(f2) -> 0
    | Autre(f1), Autre(f2) -> 0
    | Carre(f1), _ -> 1
    | Rectangle(f1), Carre(f2) -> -1
    | Rectangle(f1), _ -> 1
    | Losange(f1), Carre(f2) -> -1
    | Losange(f1), Rectangle(f2) -> -1
    | Losange(f1), _ -> 1
    | _ -> -1
;;

let () =
  let p1 = { x = 0; y = 0} in
  let p2 = { x = 4; y = 0} in
  let p3 = { x = 4; y = 1} in
  let p4 = { x = 0; y = 1} in
  let quadrilatere1 = new_quadrilatere p1 p2 p3 p4 in
  let f1 = to_figure quadrilatere1 in

  let p5 = { x = 0; y = 0} in
  let p6 = { x = 1; y = 0} in
  let p7 = { x = 1; y = 1} in
  let p8 = { x = 0; y = 1} in
  let quadrilatere2 = new_quadrilatere p5 p6 p7 p8 in
  let f2 = to_figure quadrilatere2 in

  Printf.printf "%d" ( compare f1 f2)
;;

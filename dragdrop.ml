open Graphics;;

open_graph " 300x400";;

let r = 50;;

let rec push x y =
  let event = wait_next_event [Button_down] in
  let mx = event.mouse_x in
  let my = event.mouse_y in
  if ( (mx - x) * (mx -x) + (my - y) * (my - y) < r*r) then (
    set_color red;
    fill_circle x y r;
    drag x y mx my;
  ) else (
    push x y
  )
and drag x y mx my =
  let event = wait_next_event [Button_up;Mouse_motion] in
  if ( not event.button) then (
    clear_graph();
    set_color black;
    fill_circle mx my r;
    push mx my
  ) else (
    clear_graph ();
    set_color black;
    fill_circle x y r;
    set_color red;
    fill_circle mx my r;
    drag x y event.mouse_x event.mouse_y
  )
;;

let rec jeu () = 
  set_color black;
  fill_circle 100 100 r;
  push 100 100
;;

jeu ();;
open Graphics ;;

open_graph " 1920x1080";;


let rec dragon n x y z t =
  match n with
  | 1 -> (
    moveto (int_of_float x) (int_of_float y);
    lineto (int_of_float z) (int_of_float t)
  )
  | _ -> (
    let u = (x +. z) /. 2. +. (t -. y)/. 2. in
    let v = (y +. t) /. 2. -. (z -. x)/. 2. in
    (
      dragon (n-1) x y u v;
      dragon (n-1) z t u v
    )
  )

;;

let () =  
  dragon 20 0. 0. 1920. 1080. ;
  ignore (Graphics.wait_next_event [Graphics.Button_down])
;;


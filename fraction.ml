type frac = { num : int; denom : int} ;;

let rec pgcd a b =
  match a,b with
  | _, 0 -> a
  | _, _ -> pgcd b (a mod b)
;;

let simpl frac =
  let div = pgcd frac.num frac.denom in
  { num = frac.num / div; denom = frac.denom / div}
;;

let ajoute_fracs q1 q2 = 
  let q = {num= q1.num * q2.denom + q2.num * q1.denom; denom = q1.denom * q2.denom} in
  simpl q
;;

let inverse q =
  { num = q.denom; denom = q.num}
;;

let mult_constante c f =
  simpl { num = f.num * c ; denom = f.denom }
;;

type expr = 
  | Int of int
  | Var of string
;;

type fracexpr = { enum : expr; edenom : expr } ;;

let simpl_expr q =
  match q.enum, q.edenom with
  | Int x, Int y -> (
    let div = pgcd x y in
    { enum = Int (x / div) ; edenom = Int(y/ div)}
  )
  | _-> q
;;

let rec regle_de_trois q1 q2 =
  match q1.enum, q1.edenom, q2.enum, q2.edenom with
  | Var a, Int b, Int c, Int d ->
    let f1 = { num = c; denom = d} in 
    let f2 = simpl (mult_constante b f1) in
      { enum = Int(f2.num); edenom = Int(f2.denom)}
  | Int a, Var b, Int c, Int d ->
    let f1 = inverse { num = c; denom = d} in 
    let f2 = simpl (mult_constante a f1) in
      { enum = Int(f2.num); edenom = Int(f2.denom)}
  | _ -> regle_de_trois q2 q1
;;

(* de : https://www.lri.fr/~conchon/IPF/TP/2017-2018/2/solutions/frac.ml la partie test*)
let () =
    assert (regle_de_trois { enum = Int 2; edenom = Int 3 } { enum = Int 4; edenom = Var "x" } = { enum = Int 6; edenom = Int 1 });
    assert (regle_de_trois { enum = Int 4; edenom = Int 3 } { enum = Var "x"; edenom = Int 5 } = { enum = Int 20; edenom = Int 3 });
    assert (regle_de_trois { enum = Var "x"; edenom = Int 3 } { enum = Int 8; edenom = Int 10 } = { enum = Int 12; edenom = Int 5 })